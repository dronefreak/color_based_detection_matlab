# README #

This MATLAB helps in detection of red objects in the frame from the webcam/external camera video input.  
Red colour mask is what I did. You guys can go ahead and change the mask to any colour required.

### Setup ###

* Clone the repo
* Run the code in MATLAB preferably in `R2013b` 
* See the magic !!

### Contribution guidelines ###

* Writing tests
* Color masks
* Code review